import React from 'react';
//import Container from 'react-bootstrap/Container';
//import Banner from '../components/Banner';
//import Content from '../components/Content';
import Footer from '../components/Footer';
import { Container,Image,Row,Col } from 'react-bootstrap';
import books from '../images/books.jpg'

function EducationPage(props) {
    return (
        <div>
              

            <Container className="cont" fluid={true}>
                <div className="justify-content-center">
                    <h1>Образование</h1>
                 <Image classname="img-view" style={{ height: 500, width: 750, borderRadius: 40 }} src={books}></Image>
                 <p style={{textAlign:'center',marginLeft:300,marginRight:300,display:'absolute'}}>
                        Образовaние — единый целенаправленный процесс воспитания и обучения,
                         а также совокупность приобретаемых знаний, умений, навыков, ценностных установок,
                          функций, опыта деятельности и компетенций.Образование это и есть процесс передачи этих знаний,
                           накопленных в культуре, новым поколениям. Образование целенаправленно осуществляется обществом 
                           через учебные заведения: детские сады, школы, колледжи, университеты и другие заведения,
                            что однако не исключает возможность и самообразования, 
                          особенно в связи с широкой доступностью интернета.
                 </p>

                    <Row style={{
                        marginLeft: 5, marginRight: 5, padding: 0, marginTop: 10
                    }}>
                        <Col style={{ backgroundColor: ' #b3d9ff', borderRadius: 20, marginLeft: 3, marginRight: 3 }}>
                            <h4>Домашняя задания</h4>
                            <p>
                                Индивидуальная задания,Групповая задания
                     </p>
                        </Col>

                        <Col style={{ backgroundColor: '#ffd6cc', borderRadius: 20, marginLeft: 3, marginRight: 3 }}>
                            <h4>Онлайн уроки</h4>
                            <p>
                                Дистанционные занятия
                     </p>
                        </Col>

                        <Col style={{ backgroundColor: ' #ccccff', borderRadius: 20, marginLeft: 3, marginRight: 3 }}>
                            <h4>Дополнительные занятия</h4>
                            <p>
                                Репетиторство(онлайн,дома)
                     </p>
                        </Col>
                    </Row>
                 </div>

                
            </Container>
            <Footer />
        </div>

    );
}

export default EducationPage;
