import React from 'react';
//import Container from 'react-bootstrap/Container';
import Banner from '../components/Banner';
import Content from '../components/Content';
import Footer from '../components/Footer';

function AboutPage(props) {
    return(
      <div>
          <Banner title={props.title}/>

          <Content>
               
                <h4> Асар - это социальная платформа, основанная на безвозмездной помощи и поддержки людей друг-другу.</h4>
                <h2>Основная задача</h2>

                 <h4>Организация групп людей готовых помочь друг-другу в различных сферах жизни, 
                 при этом исключая обыденные способы денежных оплат.</h4>
                 <h2>Цель проекта</h2>
                <h4>Целью данной работы является разработка приложения для ОС iOS и Android,
                     назначением которой будет организация групп людей готовых помочь друг-другу в различных сферах жизни, 
                     при этом исключая обыденные способы денежных оплат.
                     Пользователями данного приложения будут студенты и все желающие люди.</h4>
                     


             
          </Content>

          <Footer/>
      </div>

    );
}

export default AboutPage;
