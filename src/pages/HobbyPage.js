import React from 'react';
//import Container from 'react-bootstrap/Container';

import Footer from '../components/Footer';
import { Container, Image,Row,Col} from 'react-bootstrap';
import hobby from '../images/hobby.jpg'

function HobbyPage(props) {
    return (
        <div>
            <Container className="cont" fluid={true}>
                <div className="justify-content-center">
                    <h1>Хобби</h1>
                    <Image classname="img-view" style={{ height: 500, width: 750, borderRadius: 40 }} src={hobby}></Image>
                    <p style={{ textAlign: 'center', marginLeft: 300, marginRight: 300, display: 'absolute' }}>
                        Хобби — это занятие для души, которое делает нашу жизнь наполненной, улучшает настроение,
                         заряжает энергией и доставляет удовольствие. В подростковом возрасте оно приучает к аккуратности, 
                         развивает любознательность, воображение.
                         Увлечения взрослого помогают ему вернуться в счастливое детство и разбудить в себе тот восторг.
                         Настоящие счастливцы – люди, которые сумели сделать свое хобби профессией.
                 </p>
                    <Row style={{
                        marginLeft: 5, marginRight: 5, padding: 0, marginTop: 10
                    }}>
                        <Col style={{ backgroundColor: ' #b3d9ff', borderRadius: 20, marginLeft: 3, marginRight: 3 }}>
                            <h4>Развлечение</h4>
                            <p>
                                Прогулка в парке,Кинотеатры,Театры,Музеи и выставки,Охота,Рыбалка,Игры,Поход в горы
                     </p>
                        </Col>

                        <Col style={{ backgroundColor: '#ffd6cc', borderRadius: 20, marginLeft: 3, marginRight: 3 }}>
                            <h4>Спорт</h4>
                            <p>
                                Фитнес,Сноуборд,Пейнтбол,Баскетбол,Волейбол,Теннис,Футбол
                     </p>
                        </Col>

                        
                    </Row>
                </div>
            </Container>

            <Footer />
        </div>

    );
}

export default HobbyPage;
