import React from 'react';
//import Container from 'react-bootstrap/Container';

import Footer from '../components/Footer';
import { Container, Image,Row,Col } from 'react-bootstrap';
import help from '../images/help.jpg'

function HelpPage(props) {
    return (
        <div>

            <Container className="cont" fluid={true}>
                <div className="justify-content-center">
                    <h1>Помощь</h1>
                    <Image classname="img-view" style={{ height: 500, width: 750, borderRadius: 40 }} src={help}></Image>
                    <p style={{ textAlign: 'center', marginLeft: 300, marginRight: 300, display: 'block' }}>
                        это действия, направленные на содействие, участие, поддержку нуждающихся. Это фундаментальное понятие общечеловеческого сообщества,
                         через которое характеризуется уровень развития личности. Считается, что «помощь есть стук в будущее», что помогать надо везде,
                          где может рука проникнуть, мысль пролететь. Посредством помощи можно принести пользу,
                         облегчить чью-то судьбу, достичь цели спасения собственной души: помогая другим, мы помогаем себе.
                 </p>

                    <Row style={{
                        marginLeft: 5, marginRight: 5, padding: 0, marginTop: 10
                    }}>
                        <Col style={{ backgroundColor: ' #b3d9ff', borderRadius: 20, marginLeft: 3, marginRight: 3 }}>
                            <h4>Работа</h4>
                            <p>
                                Найти работу,Подработка,Удаленная работа
                     </p>
                        </Col>

                        <Col style={{ backgroundColor: '#ffd6cc', borderRadius: 20, marginLeft: 3, marginRight: 3 }}>
                            <h4>Благотворительность</h4>
                            <p>
                               Благотворительные организации,Помощь нуждающим
                     </p>
                        </Col>

                        <Col style={{ backgroundColor: ' #ccccff', borderRadius: 20, marginLeft: 3, marginRight: 3 }}>
                             <h4>Волонтерство</h4>
                            <p>
                                Участия в мероприятиях,Добровольная помощь
                     </p>
                        </Col>
                    </Row>

                </div>
            </Container>

            <Footer />
        </div>

    );
}

export default HelpPage;
