import React from 'react';
import Banner from '../components/Banner';
import Circle from '../components/Circle';
import Footer from '../components/Footer';




function HomePage(props) {

    return(
        <div>
            <Banner title={props.title} subTitle={props.subTitle} text={props.text} />
            <div className="mt-5" style={{ width: 'absolute', height: 150, backgroundColor:'#9999ff',display:'block',borderRadius:100}}>
            <h2 style={{textAlign:'center',color:'bold'}}>Наша Миссия</h2>
                <h5 style={{textAlign:'center',color:'bold',marginLeft:100,marginRight:50}}>Мы возвращаем людям самое ценное — свободу выбора.
                     Мы связываем их напрямую, потому что знаем, как важно иметь возможность самостоятельно принимать решения. 
                    Каждый день мы защищаем миллионы людей от диктата и манипуляций глобальных корпораций, навязывающих свои условия.
                    </h5>
            
            </div>
            <Circle />
            
            <Footer />
        </div>
        
    );
    
}

export default HomePage;
