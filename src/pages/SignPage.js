import React from 'react';
import Banner from '../components/Banner';
import RegisterPage from './RegisterPage';
import { Link } from 'react-router-dom';
import Content from '../components/Content';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Footer from '../components/Footer';

import AdminPage from '../admin/AdminPage'



class SignPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

            email: '',
            password: '',
            disabled: 'false',
            emailSent: 'null',
        }
    }


    handleChange = (event) => {
        console.log(event);

        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        this.setState({
            [name]: value
        })

    }

    handleSubmit = (event) => {
        event.preventDefault();

        this.setState({
            disabled: true,
           

        });
    }

    render() {
        return (
            <div>
                <Banner title={this.props.title} />
                <Content>
                    <Form onSubmit={this.handleSubmit}>


                        <Form.Group>
                            <Form.Label htmlFor="email">Почта</Form.Label>
                            <Form.Control id="email" name="email" type="email" value={this.state.email} onChange={this.handleChange} ></Form.Control>
                        </Form.Group>


                        <Form.Group>
                            <Form.Label htmlFor="password">Пароль</Form.Label>
                            <Form.Control id="password" name="password" type="password" value={this.state.password} onChange={this.handleChange} ></Form.Control>
                        </Form.Group>

                        <Button className="d-inline-block" variant="primary" type="submit" disabled={this.state.disabled}>Войти</Button>
                        <Link className="p-0 d-flex justify-content-end"  href={RegisterPage} alt="#" to="reg" md={3}>Регистрация</Link>
                        <Link className="p-0 d-flex justify-content-end" href={AdminPage} alt="#" to="adminpage" md={3}>admin</Link>

                        {this.state.emailSent === true && <p className="d-inline success-mg">(-_-)</p>}
                        {this.state.emailSent === false && <p className="d-inline error-msg">ошибка!</p>}

                    </Form>
                </Content>
                   
                   <Footer/>
            </div>
        );
    }
}
export default SignPage;


