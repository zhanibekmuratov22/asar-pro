import React from 'react';
import HeaderContact from '../components/HeaderContact';
import Banner from '../components/Banner';
import Footer from '../components/Footer';
//import Content from '../components/Content';
import { Container,Row,Col,Jumbotron } from 'react-bootstrap';
//import Review from '../components/Review';
//import { Link } from 'react-router-dom';


//import bgcontact from '../images/bgcontact.jpg';


function ContactsPage(props) {
    return (
        
            <div >
                <HeaderContact/>
                <Banner title={props.title}/>

            <Jumbotron>
                <Container fluid={true}>
                    <Row className=" justify-content-center">
                        <h2>Напишите нам</h2>
                    </Row>
                    <Row className=" justify-content-md-center-between p-4">
                           
                        <Col ><h5>Сотрудничество и реклама</h5>
                            <div>&nbsp;</div>asarpro@gmail.com</Col>
                        <Col ><h5>PR-департамент</h5>
                            <div>&nbsp;</div>asarpro@gmail.com</Col>
                        <Col ><h5>Служба поддержки</h5><div>&nbsp;</div>support.asarpro@gmail.com</Col>
                        <Col ><h5>Взаимодействие с государственными органами</h5>
                            <div>&nbsp;</div>asarpro@gmail.com</Col>
                    
                    </Row>

                    <Row className=" border-top justify-content-center">
                        <h2>Наш офис</h2>
                    </Row>
                    <Col>
                    <h5>г.Алматы,Манаса 34/1</h5>
                    </Col>
                </Container>
            </Jumbotron>
                
                
            
          

               <Footer/>
           
        </div>
        
    );
}

export default ContactsPage;
