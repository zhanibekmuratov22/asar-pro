import React from 'react';
import Banner from '../components/Banner';
import Content from '../components/Content';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import FormControl from 'react-bootstrap/FormControl';
import Footer from '../components/Footer';
//import ReactDatePicker from 'react-datepicker';
//import DatePicker from "react-datepicker";
//import DatePicker from 'react-bootstrap/DatePicker';






class RegisterPage extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            name: '',
            surname:'',
            email:'',
            birthday:new Date(),
            phone:'',
            password:'',
            disabled:'false',
            emailSent:'null',
        }
    }


    handleChange = (event,date) => {
      console.log(event);

      const target = event.target;
      const value = target.type === 'checkbox' ? target.checked : target.value;
      const name = target.name;

      this.setState({
          [name]: value,
          birthday:date
      })

    }

    handleSubmit = (event) =>{
        event.preventDefault();

        this.setState({
            disabled:true,
          
            
        });
    }

    render(){
        return (
        <div>
            <Banner title={this.props.title} />
            <Content>
             <Form onSubmit={this.handleSubmit}>
                 <Form.Group>
                     <Form.Label htmlFor="name">Имя</Form.Label>
                     <Form.Control id="name" name="name" type="text" value={this.state.name} onChange={this.handleChange} ></Form.Control>
                 </Form.Group>

                    <Form.Group>
                        <Form.Label htmlFor="surname">Фамилия</Form.Label>
                        <Form.Control id="surname" name="surname" type="text" value={this.state.surname} onChange={this.handleChange} ></Form.Control>
                    </Form.Group>

                    <Form.Group>
                        <Form.Label htmlFor="email">Почта</Form.Label>
                        <Form.Control id="email" name="email" type="email" value={this.state.email} onChange={this.handleChange} ></Form.Control>
                    </Form.Group>

                    <Form.Group>
                        <Form.Label htmlFor="birthday">Дата рождения</Form.Label>
                        <FormControl id="birthday" name="birthday" type="date" value={this.state.birthday} onChange={this.handleChange} ></FormControl>
                    </Form.Group>
                        <Form.Group>
                            <Form.Label htmlFor="phone">Телефон</Form.Label>
                            <FormControl id="phone" name="phone" type="phone-number"  defaultCountry={'KZ'}
                                defaultValue={'+7 777-777-7777'} value={this.state.phone} onChange={this.handleChange} ></FormControl>
                        </Form.Group>

                        <Form.Group>
                            <Form.Label htmlFor="password">Пароль</Form.Label>
                            <Form.Control id="password" name="password" type="password" value={this.state.password} onChange={this.handleChange} ></Form.Control>
                        </Form.Group>

                    <Button className="d-inline-block" variant="primary" type="submit" disabled={this.state.disabled}>Регистрация</Button>

                    {this.state.emailSent === true && <p className="d-inline success-msg">Регистрация успешно</p>}
                    {this.state.emailSent === false && <p className="d-inline error-msg">Ошибка!</p>}
                 
             </Form>
            </Content>
           <Footer/>
        </div>
    );
}
}

export default RegisterPage;
