import React from 'react';
//import Container from 'react-bootstrap/Container';

import Footer from '../components/Footer';
import { Container, Image,Col,Row} from 'react-bootstrap';
import other from '../images/other.jpg'

function OtherPage(props) {
    return (
        <div>
            <Container className="cont" fluid={true}>
                <div className="justify-content-center">
                    <h1>Другие</h1>
                    <Image classname="img-view" style={{ height: 500, width: 750, borderRadius: 40 }} src={other}></Image>
                    <p style={{ textAlign: 'center', marginLeft: 300, marginRight: 300, display: 'absolute' }}>
                        Здесь вы сможете подробнее ознакомится с другими услугами.
                 </p>
                    <Row style={{
                        marginLeft: 5, marginRight: 5, padding: 0, marginTop: 10}}>
                        <Col style={{ backgroundColor: ' #b3d9ff', borderRadius: 20, marginLeft: 3, marginRight: 3}}>
                        <h4>Курьерские услуги</h4>
                        <p>
                                Купить и доставить,Срочная доставка,Другая посылка
                     </p>
                 </Col>
                   
                        <Col style={{ backgroundColor: '#ffd6cc', borderRadius: 20, marginLeft: 3, marginRight: 3}}><h4>Ремонт и строительство</h4>
                        <p>
                                Мастер на час,Ремонт под ключ,Сантехнические работы,Электромонтажные работы,Сборка и ремонт мебели
                     </p>
                   </Col>
                    
                        <Col style={{ backgroundColor: ' #ccccff', borderRadius: 20, marginLeft: 3, marginRight: 3}}> <h4>Уборка и помощь по хозяйству</h4>
                        <p>
                                Химчистка,Сиделки,Няни,Уход за животными,Приготовление еды,Вынос мусора,Генеральная уборка
                     </p>
                    </Col>
                    </Row>
                    <Row style={{ marginLeft: 5, marginRight: 5, padding: 0, marginTop: 10 }}>
                        <Col style={{ backgroundColor: '#ffffe6', borderRadius: 20, marginLeft: 3, marginRight: 3 }}><h4>Установка и ремонт техники</h4>
                        <p>
                                Холодильники и морозильные камеры,Стиральные и сушильные машины,Водонагреватели, бойлеры, котлы, колонки
                     </p>
                   </Col>
                        <Col style={{ backgroundColor: '#e6fff2', borderRadius: 20, marginLeft: 3, marginRight: 3 }}><h4>Мероприятия и промоакции</h4>
                        <p>
                                Помощь в проведении мероприятий,Промоутер,Тамада, ведущий, аниматор
                     </p>
                    </Col >
                        <Col style={{ backgroundColor: ' #ffe6ff', borderRadius: 20, marginLeft: 3, marginRight: 3 }}><h4>Красота и здоровье</h4>
                            <p>
                                Парикмахерские услуги,Косметология, макияж,Персональный тренер
                     </p>
                        </Col>
                    </Row>

                     





                 
                </div>
            </Container>

            <Footer />
        </div>

    );
}

export default OtherPage;
