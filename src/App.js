import React from 'react';
import Container from 'react-bootstrap/Container';
import NavBar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import {BrowserRouter as Router,Route,Link} from "react-router-dom";
import './App2.css';
import HomePage from './pages/HomePage';
import AboutPage from './pages/AboutPage';
import ContactsPage from './pages/ContactsPage';
import QaPage from './pages/QaPage';
//import SignPage from './pages/SignPage';
//import RegisterPage from './pages/RegisterPage';
import EducationPage from './pages/EducationPage';
import HobbyPage from './pages/HobbyPage';
import HelpPage from './pages/HelpPage';
import OtherPage from './pages/OtherPage';
import logo from './images/logo3.png';
//import AdminPage from './admin/AdminPage';
//import UsersPage from './admin/UsersPage';
//import DatabasePage from './admin/DatabasePage';






class App2 extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            title:"Asar",
            imgSrc:logo,
            loggedIn:false,
            headerLinks:[
                {title:'Главная',path:'/'},
                { title: 'О нас', path: '/about'},
                { title: 'Контакты', path: '/contacts'},
                { title: 'Вопросы-Ответы', path: '/qa' },
                { title: 'Войти', path: '/signin'}
            ],
            home:{
                title:'ASAR',
            subTitle:'#BizBirgemiz',
            text:'Главная фишка - нет мошенников!'},
            about: {
                title: 'О Нас'
            },
            contacts: {
                title: 'Наши Контакты'
            },
            qa:{
               title:'Вопросы-Ответы'
            },
            sign:{
                title:'Войти в систему'
            },
            reg: {
                title: 'Регистрация'
            },
            educationpage:{
                title:'Образование'
            },
            hobbypage: {
                title: 'Хобби'
            },
            helppage: {
                title: 'Помощь'
            },
            otherpage: {
                title: 'Другие'
            }, adminpage: {
                title: 'Admin page'
            },
            userspage:{
                title:'Users page'
            },
            databasepage:{
                title:'Database page'
            }
        }
        
        
    }

    

    componentDidMount() {
        if (document.cookie.split(';').filter((item) => item.trim().startsWith('logedIn=')).length) {
            this.setState({ loggedIn: true })
        }
        window.setInterval(() => {
            if (document.cookie.split(';').filter((item) => item.trim().startsWith('logedIn=')).length) {
                this.setState({ loggedIn: true })
            }
            else {
                this.setState({ loggedIn: false })
            }
        }, 500)
    }


    
    render(){
        
      return (
        <Router>
        <Container className="p-0" fluid={true}>
            
                  <NavBar className="border-bottom " bg="light" expand="lg" sticky="top" fixed="top" variant="light" >
                      <Link to="/"> <NavBar.Brand ><img src={logo} alt="logo" to="/qa" padding=" 7px 14px" width="100px" mt="-7" /></NavBar.Brand>
                        </Link>
                     
              <NavBar.Toggle className="border-0" aria-controls="navbar-toggle"/>
              <NavBar.Collapse id="navbar-toggle">
                  <Nav className="ml-auto">
                      <Link className="nav-link" to="/">Главная</Link>
                      <Link className="nav-link" to="/about">О Нас</Link>
                      <Link className="nav-link" to="/contacts">Контакты</Link>
                              <Link className="nav-link" to="/qa">Вопросы-Ответы</Link>
                              
                                
                                 
                      
                      
                  </Nav>
              </NavBar.Collapse>
            </NavBar>
                  <Route path="/" exact render={() => <HomePage title={this.state.home.title} subTitle={this.state.home.subTitle} text={this.state.home.text} />} />
                  <Route path="/about" render={() => <AboutPage title={this.state.about.title} />} />
                  <Route path="/contacts" render={() => <ContactsPage title={this.state.contacts.title} />} />
                  <Route path="/qa" render={() => <QaPage title={this.state.qa.title} />} />
               
                  <Route path="/educationpage" render={() => <EducationPage title={this.state.educationpage.title} />} />
                  <Route path="/hobbypage" render={() => <HobbyPage title={this.state.hobbypage.title} />} />
                  <Route path="/helppage" render={() => <HelpPage title={this.state.helppage.title} />} />
                  <Route path="/otherpage" render={() => <OtherPage title={this.state.otherpage.title} />} />
                  

                  

                  

        

        </Container>
        </Router>
             );

    }
}
 export default App2;