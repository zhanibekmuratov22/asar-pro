import React from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Image from 'react-bootstrap/Image';
import appstore from '../images/appstore.png';
import playmarket from '../images/playmarket.png';
import { Link } from 'react-router-dom';
import { SocialIcon } from 'react-social-icons';



function Footer(){
   return(
     <footer className="mt-5">
         <Container fluid={true}>
        
             <Row className="border-top justify-content-between p-3">
                 <Col className="p-0" md={3} sm={12}>Мы в социальных сетях!{"  "}
             <SocialIcon network="facebook" style={{ height: 25, width: 25 }} url="http://facebook.com" target="_blank" />{" "}
             <SocialIcon network="instagram" style={{ height: 25, width: 25 }} url="http://instagram.com" target="_blank" />{" "}
             <SocialIcon network="vk" style={{ height: 25, width: 25 }} url="http://vk.com" target="_blank" />{" "}
             <SocialIcon network="twitter" style={{ height: 25, width: 25 }} url="http://twitter.com" target="_blank" />{" "}
             
                 </Col>
                   <Col className="p-0 d-flex justify-content-end"  md={4}>Скачайте мобильное приложение на IOS и Android.</Col>
                   <Col className="p-0" md={3} sm={10}>
             <Link href={'https://www.apple.com/ru/ios/app-store/'}  target="_blank" rel="noopener noreferrer">
               <Image src={appstore} padding=" 7px 14px" width="100px" className="appstoreIcon"/>
               </Link>
             <Link href={'https://play.google.com/store/apps?hl=ru'}  target="_blank" rel="noopener noreferrer">
                 <Image src={playmarket} padding=" 7px 14px" width="100px" className="playmarketIcon"/>
               </Link>
               
                     
            
                   </Col>
             </Row>
         </Container>
     </footer>
   );
}

export default Footer;