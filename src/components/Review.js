import React,{Component} from 'react';
import {format} from 'date-fns';

class Review extends Component{
  
    state = {
        comments:[],
        form:{
            email:'',
            name:'',
            comment:''
        }
    }

    componentDidMount(){
        if (localStorage.getItem('state')) {
            this.setState({ ...JSON.parse(localStorage.getItem('state')) })
        } 
    }
    addComment = () =>{
        this.setState({
            comments:[
                ...this.state.comments,
                {
                    id: this.state.comments.length ? this.state.comments.reduce((p, c) => p.id > c.id ? p : c).id + 1 : 1,
                    email: this.state.form.email,
                    name: this.state.form.name,
                    comment: this.state.form.comment,
                    date:new Date()
                }

            ],
            form:{
                email:'',
                name:'',
                comment:''
            }   
        } , () => localStorage.setItem('state', JSON.stringify(this.state)))
    }

    removeComment = (id) => {
        this.setState({
            comments: this.state.comments.filter(comment => comment.id !== id)
        },() => localStorage.setItem('state', JSON.stringify(this.state)))
    }

    handleChange = (e) =>{
        console.log(e.target.email)
        this.setState({
            form:{
                ...this.state.form,
                [e.target.email]: e.target.value,
            }
        })
    }

    render(){
        return(
            <div>
                {this.state.comments.map(comment => <div key={comment.id}>
                    <span style={{ fontStyle: 'italic' }}>{comment.id} - {format(comment.date, 'DD/MM/YYYY')}: </span>
                    <strong>{comment.email}, </strong>
                    <strong>{comment.name}, </strong>
                    <span>{comment.comment}</span>
                    <button onClick={this.removeComment.bind(null, comment.id)}>Удалить комментарий</button>

            </div>
            )}
            <div>
                <label>Email: 
                    <input type="email" value={this.state.email} name="email" onChange={this.handleChange}></input>
                </label>
                    <label>Name:
                    <input type="text" value={this.state.name} name="name" onChange={this.handleChange}></input>
                    </label>
                    <label>comments: <textarea type="number" value={this.state.comments} name="comment" onChange={this.handleChange}>
                        </textarea></label>
                    <button onClick={this.addComment}>Добавить комментарий</button>
            
            </div>
            </div>
        )
                }

}

export default Review;