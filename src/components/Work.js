import React from 'react';
import { Carousel,Row,Col,Container } from 'react-bootstrap';
//import phone from '../images/phone.jpeg';
import chat from '../images/chat.jpeg'
import chat2 from '../images/chat2.jpeg'
import chat3 from '../images/chat3.jpeg'




function Work(props) {
    return (
        <Container fluid={true}>
            <Row className="border-top justify-content-center">
                <Col className="how-work">
                    <h4>Как работает приложение Асар?</h4>
                    <ul style={{marginLeft:10,listStyleType:'none'}}>
                        <li >1.Регистрация</li>
                        <li >2.Выбрать услугу или секцию </li>
                        <li>3.Также можете добавить свою публикацию</li>
                        <li>4.Отправить сообщение неравнодушному человеку</li>
                        <li>5.Можете сохранить публикацию в разделе избранные</li>
                       <li>6.Помогать друг-другу и оставить отзыв</li>
                    </ul>
                    
                    
                    
                </Col>

                <Col className="p-0"  >
                <Carousel>
                    <Carousel.Item>
                            <img className="work-img" src={chat3} alt="First"></img>
                        
                    </Carousel.Item>
                        <Carousel.Item>
                            <img className="work-img" src={chat2} alt="Second"></img>
                            
                        </Carousel.Item>
                        <Carousel.Item>
                            <img className="work-img" src={chat} alt="Third"></img>
                            
                        </Carousel.Item>
                </Carousel>
                </Col>
            </Row>
        </Container>
    );

}

export default Work;