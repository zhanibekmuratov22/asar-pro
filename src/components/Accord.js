import React from 'react';
import { Accordion,Button,Card,Container,Row,Col } from 'react-bootstrap';

function Accord(params) {
    return(
        <div >

            <Container fluid={false}>
                <Row className="p-0 justify-content-center">
                    <h3 classname="h3-qa">Часто задаваемые вопросы</h3>
                    <Col>
                        <Accordion defaultActiveKey="0">
                            <Card>
                                <Card.Header>
                                    <Accordion.Toggle as={Button} variant="link" eventKey="0">
                                        Это действительно без денег делается?
      </Accordion.Toggle>
                                </Card.Header>
                                <Accordion.Collapse eventKey="0">
                                    <Card.Body>Да,это взаимная помощь!</Card.Body>
                                </Accordion.Collapse>
                            </Card>
                            <Card>
                                <Card.Header>
                                    <Accordion.Toggle as={Button} variant="link" eventKey="1">
                                        Как удалить аккаунт?
      </Accordion.Toggle>
                                </Card.Header>
                                <Accordion.Collapse eventKey="1">
                                    <Card.Body>Отправьте скан заявления на удаление аккаунта в произвольной форме на электронную почту support@asarpro.com.</Card.Body>
                                </Accordion.Collapse>
                            </Card>
                            <Card>
                                <Card.Header>
                                    <Accordion.Toggle as={Button} variant="link" eventKey="2">
                                    Я получил отрицательную оценку/отзыв,требую удалить.
      </Accordion.Toggle>
                                </Card.Header>
                                <Accordion.Collapse eventKey="2">
                                    <Card.Body>
                                        К сожалению, оценки не подлежат корректировке или удалению, так как являются субьективным мнением человека.
                                    </Card.Body>
                                </Accordion.Collapse>
                            </Card>
                            <Card>
                                <Card.Header>
                                    <Accordion.Toggle as={Button} variant="link" eventKey="3">
                                       Что делать,если у вас произошел конфликт?
      </Accordion.Toggle>
                                </Card.Header>
                                <Accordion.Collapse eventKey="3">
                                    <Card.Body>
                                        Откройте боковое меню, зайдите в раздел «Служба поддержки» далее выберите «Написать сообщение», затем  суть конфликта. Или вы можете позвонить в нашу службу поддержки по номеру  7070.
                                    </Card.Body>
                                </Accordion.Collapse>
                            </Card>
                            
                        </Accordion>
                    </Col>
                </Row>
            </Container>
            
            
        </div>
    );
}



export default Accord;