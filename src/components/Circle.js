import React from 'react'
import Card from '../components/Card';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import books from '../images/books.jpg'
import hobby from '../images/hobby.jpg'
import help from '../images/help.jpg'
import other from '../images/other.jpg'



class Circle extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            items: [
                {
                    id: 0,
                    title: 'Образование',
                    subTitle: 'ДЗ,Онлайн,Занятие',
                    imgSrc: books,
                    link: 'educationpage',
                    selected: false
                },
                {
                    id: 1,
                    title: 'Хобби',
                    subTitle: 'Развлечение,Спорт',
                    imgSrc: hobby,
                    link: 'hobbypage',
                    selected: false
                },
                {
                    id: 2,
                    title: 'Помощь',
                    subTitle: 'Работа,Благотворительность,Волонтерство',
                    imgSrc: help,
                    link: 'helppage',
                    selected: false
                },
                {
                    id: 3,
                    title: 'Другие',
                    subTitle: '....',
                    imgSrc: other,
                    link: 'otherpage',
                    selected: false
                },
            ]
        }
    }


    handleCardClick = (id, card) => {
        

        let items = [...this.state.items];

        items[id].selected = items[id].selected ? false : true;

        items.forEach(item => {
            if (item.id !== id) {
                item.selected = false;
            }
        });

        this.setState({
            items
        });
    }


    makeItems = (items) => {
        return items.map(item => {
            return <Card item={item} click={(e => this.handleCardClick(item.id, e))} key={item.id} />
        })
    }


    render() {
        return (
            <Container fluid={true}>
                <Row className="justify-content-around">
                    {this.makeItems(this.state.items)}
                </Row>
            </Container>
        );
    }

}
export default Circle;
