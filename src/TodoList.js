import React, { Component } from 'react';
import './App.css';

class TodoList extends React.Component{
    constructor(){
        super();
        this.state = {edit:false};

    };
    edit() {
        this.setState({ edit: true });
    }
    save() {
        var value = this.textInput.value;
        this.props.updateText(value, this.props.index);
        this.setState({ edit: false });
    }

    rendNorm() {
        return (
            <div className="box">
                <div className="text">{this.props.children}</div>
                <button onClick={this.edit.bind(this)} className="btn light">Редактировать</button>
                <button onClick={() => { this.props.deleteBlock(this.props.index) }} className="btn red">Удалить</button>
                <button className="btn light">Выполненно</button>
            </div>
        );
    }
    rendEdit() {
        return (
            <div className="box">
                <textarea ref={(input) => { this.textInput = input; }} defaultValue={this.props.children}></textarea>
                <button onClick={this.save.bind(this)} className="btn success">Сохранить</button>
            </div>
        );
    }
    render(){
        if(this.state.edit){
            return this.rendEdit();
        } 
        else{
            return this.rendNorm();
        }
    }
}

export default TodoList;

