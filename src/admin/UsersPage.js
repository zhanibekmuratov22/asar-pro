import React from 'react';
import {Row,Col,Container} from 'react-bootstrap';
import { withRouter } from "react-router";
import Sidebar from "./Sidebar";
import './Dashboard.css'
import Footer from '../components/Footer'


const Users = props=> {
    
    return(
        <div>
            <Container fluid>
                <Row>
                    <Col xs={2} id="sidebar-wrapper">
                        <Sidebar />
                    </Col>
                    <Col xs={10} id="page-content-wrapper">
                        <h1>Welcome to Users</h1>
                    </Col>
                </Row>

            </Container>
            <Footer />
        </div>
    );

}
const UsersPage = withRouter(Users);

export default UsersPage;