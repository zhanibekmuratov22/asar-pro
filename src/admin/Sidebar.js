import React from "react";
import  Nav from "react-bootstrap/Nav";
import  Navbar from "react-bootstrap/Navbar";
import { BrowserRouter as Router,Route, Link } from "react-router-dom";
import './Dashboard.css'
import AdminPage from '../admin/AdminPage'
import UsersPage from '../admin/UsersPage'
import DatabasePage from '../admin/DatabasePage'



class SideBar extends React.Component{
    constructor(props){
        super(props);
        this.state = {
             title:"Admin",
            links:[
                { title: 'Admin', path: '/adminpage' },
                { title: 'Users', path: '/userspage'},
                { title: 'Database', path: '/databasepage'}
            ],
                
            
        }
    }
    render(){
        return(
            <Router>
                
               
                
                    <Nav className="col-md-12 d-none d-md-block bg-light sidebar" activeKey="/">
                        <Link className="nav-link" path="/adminpage">Admin</Link>
                        <Link className="nav-link"  path="/userspage" >Users</Link>
                        <Link className="nav-link"  path="/databasepage">Database</Link>
                    </Nav>

                
                
               
                

            </Router>
        );
    }
    }

    export default SideBar;