import React from "react";
import { Container, Row, Col } from "react-bootstrap";
import { withRouter } from "react-router";
import Sidebar from "./Sidebar";
import './Dashboard.css'
import Footer from '../components/Footer'


const Dash = props => {


    return (
        <div>
            <Container fluid>
                <Row>
                    <Col xs={2} id="sidebar-wrapper">
                        <Sidebar />
                    </Col>
                    <Col xs={10} id="page-content-wrapper">
                        this is a test
                    </Col>
                </Row>

            </Container>
            <Footer/>

        </div>
    );
};
const AdminPage = withRouter(Dash);
export default AdminPage