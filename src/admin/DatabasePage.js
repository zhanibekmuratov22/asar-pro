import React from 'react';
import { Row, Col, Container } from 'react-bootstrap';
import { withRouter } from "react-router";
import Sidebar from "./Sidebar";
import './Dashboard.css'
import Footer from '../components/Footer'


const Database = props => {

    return (
        <div>
            <Container fluid>
                <Row>
                    <Col xs={2} id="sidebar-wrapper">
                        <Sidebar />
                    </Col>
                    <Col xs={10} id="page-content-wrapper">
                        <h1>Welcome to Database</h1>
                    </Col>
                </Row>

            </Container>
            <Footer />
        </div>
    );

}
const DatabasePage = withRouter(Database);

export default DatabasePage;